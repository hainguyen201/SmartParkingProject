package com.hust.smartparking.repository;

import com.hust.smartparking.entity.ParkingSlot;
import org.springframework.data.repository.CrudRepository;

public interface ParkingSlotRepository extends CrudRepository<ParkingSlot, Integer> {
}
