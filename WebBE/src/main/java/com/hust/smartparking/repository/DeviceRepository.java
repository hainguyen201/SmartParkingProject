package com.hust.smartparking.repository;

import com.hust.smartparking.entity.Device;
import org.springframework.data.repository.CrudRepository;

public interface DeviceRepository extends CrudRepository<Device, Integer> {

}
