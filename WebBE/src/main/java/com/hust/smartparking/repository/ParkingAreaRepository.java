package com.hust.smartparking.repository;

import com.hust.smartparking.entity.ParkingArea;
import org.springframework.data.repository.CrudRepository;

public interface ParkingAreaRepository extends CrudRepository<ParkingArea, Integer> {
}
