package com.hust.smartparking.service;

import com.hust.smartparking.entity.Vehicle;

public interface IVehicleService  extends IGeneralService<Vehicle>{
}
