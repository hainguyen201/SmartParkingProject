package com.hust.smartparking.service;

import com.hust.smartparking.entity.ParkingArea;

public interface IParkingAreaService extends IGeneralService<ParkingArea>{
}
