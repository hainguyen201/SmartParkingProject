package com.hust.smartparking.service;

import com.hust.smartparking.entity.AppParam;

public interface IAppParamService extends IGeneralService<AppParam>{
}
